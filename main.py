# Seu código aqui
class Moradia:
    def __init__(self, area, endereco, numero, preco_imovel, tipo='Não especificado'):
        self.area = area
        self.endereco = endereco
        self.numero = numero
        self.preco_imovel = preco_imovel
        self.tipo = tipo

    def gerar_relatorio_preco(self):
        return int(self.preco_imovel / self.area)
        pass


moradia_1 = Moradia(100, 'Rua das Limeiras', 672, 250000)
print(moradia_1.__dict__)
print(moradia_1.gerar_relatorio_preco())


class Apartamento(Moradia):
    tipo = 'Apartamento'

    def __init__(self, area, endereco, numero, preco_imovel, preco_condominio, num_apt, num_elevadores):
        super().__init__(area, endereco, numero, preco_imovel, tipo=self.tipo)
        self.preco_condominio = preco_condominio
        self.num_apt = num_apt
        self.num_elevadores = num_elevadores
        if num_apt < 10:
            self.andar = int(num_apt / 10 + 1)
        else:
            self.andar = int(num_apt / 10)

    def gerar_relatorio(self):
        return f'{self.endereco} - apt {self.num_apt} - andar: {self.andar} - elevadores: {self.num_elevadores} - preco por m²: R$ {self.gerar_relatorio_preco()}'


apt_1 = Apartamento(100, 'Rua das Limeiras', 672, 500000, 700, 110, 2)
print(apt_1.__dict__)
print(apt_1.gerar_relatorio())
